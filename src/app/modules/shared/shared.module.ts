import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    CustomMaterialModule,
    NgbModule
  ],
  exports: [
    CustomMaterialModule,
    NgbModule
  ]
})
export class SharedModule { }
