import { PRODUCTOS } from './models/mock-productos';
import { Component, AfterViewInit, ViewChild, ElementRef, OnInit, AfterContentInit } from '@angular/core';
import { Producto } from './models/producto';
import { Observable, of, Subscriber, Subscription, fromEvent, concat } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterContentInit {
  title = 'mayoral';
  productos: Producto[] = PRODUCTOS;
  productsList$: Observable<Producto[]>;
  productsFiltering$: Observable<Producto[]>;
  @ViewChild('buscar', {static: true}) buscador: ElementRef;

  constructor(
  ) {
  }

  ngOnInit() {
  }

  ngAfterContentInit() {
    const searchProduct$ = fromEvent<any>(this.buscador.nativeElement, 'keyup').pipe(
      map(event => event.target.value),
      debounceTime(400),
      distinctUntilChanged(),
      switchMap(searchTerm => this.searchProductos(searchTerm))
    );
    this.productsFiltering$ = this.getProductos();
    const initializeProduct$ = this.productsFiltering$;
    this.productsList$ = concat(initializeProduct$, searchProduct$);
  }

  getProductos(): Observable<Producto[]> {
    return of(this.productos);
  }

  searchProductos(searchTerm?: string, e?: HTMLInputElement): Observable<Producto[]> {
    if (!searchTerm.trim()) {
      // if not search term, return empty hero array.
      this.getProductos();
    }
    this.productsList$ = this.productsFiltering$.pipe(
      map( products => products.filter(
        product => product.name === searchTerm
      ) )
    );
    return this.productsList$;
  }

}

