export class Producto {
    id: number;
    name: string;
    price: number;
    discount: number;
    color?: boolean;
    description: string;
    img: string;
}
