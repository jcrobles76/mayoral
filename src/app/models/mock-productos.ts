import { Producto } from './producto';

export const PRODUCTOS: Producto[] = [
    {id: 1, name: 'Polo 01', description: 'Polo manga corta combinado bloques niño', price: 9.49, discount: -50, color: true, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-57104-700-390-2.JPG?v=20190508054347'},
    {id: 2, name: 'Polo 02', description: 'Polo manga larga bandas niño', price: 14.49, discount: -50, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-57134-700-390-2.JPG?v=20190827141930'},
    {id: 3, name: 'Polo 03', description: 'Polo manga larga rayas niño', discount: -25, price: 15.19, color: true, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-07110-012-390-2.JPG?v=20190718115959'},
    {id: 4, name: 'Polo 04', description: 'Polo manga larga logotipo niño', discount: -22, price: 16.99, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-07111-075-390-2.JPG?v=20190722154542'},
    {id: 5, name: 'Polo 05', description: 'Polo manga larga estampada rojo', price: 21.00, discount: -22, color: true, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-07114-031-390-2.JPG?v=20190718122754'},
    {id: 6, name: 'Polo 06', description: 'Polo manga larga apliques niño', price: 21.00, discount: -22, color: true, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-07109-027-390-2.JPG?v=20190723155650'},
    {id: 7, name: 'Polo 07', description: 'Polo manga larga tricolor Team', price: 21.00, discount: -22, color: true, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-07112-061-390-2.JPG?v=20190723155650'},
    {id: 8, name: 'Polo 08', description: 'Polo manga larga bandas niño', price: 14.49, discount: -50, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-57134-700-390-2.JPG?v=20190827141930'},
    {id: 9, name: 'Polo 09', description: 'Polo manga larga mini estampado niño', price: 21.00, discount: -22, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog//mayoral/19-07114-031-390-2.JPG?v=20190718122754'},
    {id: 10, name: 'Polo 10', description: 'Polo manga larga negro', price: 21.00, discount: -22, img: 'https://media.mayoral.com/wcsstore/mayoral/images/catalog/mayoral/19-07112-062-800-2.JPG?v=20190718122753'},
];
