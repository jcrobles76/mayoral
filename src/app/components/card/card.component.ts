import { Component, OnInit, Input } from '@angular/core';
import { Producto } from 'src/app/models/producto';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() product: Producto;

  constructor() { }

  ngOnInit() {
  }

}
